//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "python3 $HOME/git/scripts/weather.py",				1800,		0},

	{"", "free -h | awk '/^Mem/ { print $3 }' | sed s/i//g",         	30,		0},

	{" ", "pamac checkupdates -q | wc -l",                           	6000,	        0},
	
        {"GPU: ", "nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader", 40,		0},
	
	{"CPU: ", "sensors | grep -m 1 Core | awk '{print substr($3, 2, length($3)-5)}'", 35,   0}, 

	{"", "date '+%H:%M (%a) %b %d'",					60,		0},	
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
